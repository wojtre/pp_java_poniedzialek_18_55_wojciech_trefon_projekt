



select T2.*, T3.stan, T4.nazwa, T5.miasto from samochody T2
inner join stany_samochodu T3 on T2.Stany_Samochodu_id_stanu = T3.id_stanu 
inner join klasy_samochodow T4 on T4.id_klasy = T2.Klasy_samochodow_id_klasy 
inner join wypozyczalnie T5 on T2.Wypozyczalnie_id_wypozyczalni = T5.id_wypozyczalni 
where T2.marka like '%%' AND T2.model_samochodu like '%%' AND T2.rok_produkcji>2010 AND T2.Stany_Samochodu_id_stanu=1;


select count(T1.Samochody_id_samochodu) as ilosc,T2.*, T3.stan, T4.nazwa, T5.miasto from wypozyczenia T1 inner join samochody T2
on T2.id_samochodu = T1.Samochody_id_samochodu inner join stany_samochodu T3 on T2.Stany_Samochodu_id_stanu = T3.id_stanu 
inner join klasy_samochodow T4 on T4.id_klasy = T2.Klasy_samochodow_id_klasy 
inner join wypozyczalnie T5 on T2.Wypozyczalnie_id_wypozyczalni = T5.id_wypozyczalni group by T1.Samochody_id_samochodu order by ilosc desc;