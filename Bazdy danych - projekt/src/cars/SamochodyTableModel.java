package cars;

import java.util.List;

import javax.swing.table.AbstractTableModel;

public class SamochodyTableModel extends AbstractTableModel {
    private static final int ILOSC_WYSTAPIEN = 0;
    private static final int MARKA = 1;
    private static final int MODEL = 2;
    private static final int NR_REJESTRACYJNY = 3;
    private static final int MOC_SILNIKA = 4;
    private static final int ROK_PRODUKCJI = 5;
    private static final int STAN = 6;
    private static final int WYPOZYCZALNIA = 7;
    private static final int KLASA = 8;
    private static final int RODZAJ_SILNIKA = 9;
    private String[] columnNames = { "Wypozyczen", "Marka", "Model", "Nr rej.",
            "Moc [KM]", "Rok produkcji", "Stan", "Aktualna Wypozyczalnia",
            "Klasa", "Rodzaj silnika" };
    private List<Samochod> cars;
    
    public SamochodyTableModel(List<Samochod> c) {
        cars = c;
    }
    
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public int getRowCount() {
        return cars.size();
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public Object getValueAt(int row, int col) {
        Samochod tmpCar = cars.get(row);
        switch (col)
        {
        case ILOSC_WYSTAPIEN:
            return tmpCar.getiloscWystapien();
        case MARKA:
            return tmpCar.getmarka();
        case MODEL:
            return tmpCar.getmodel();
        case NR_REJESTRACYJNY:
            return tmpCar.getnrRejestracyjny();
        case MOC_SILNIKA:
            return tmpCar.getmocSilnika();
        case ROK_PRODUKCJI:
            return tmpCar.getrokProdukcji();
        case RODZAJ_SILNIKA:
            return tmpCar.getrodzajSilnika();
        case STAN:
            return tmpCar.getStan();
        case WYPOZYCZALNIA:
            return tmpCar.getWypozyczalnia();
        case KLASA:
            return tmpCar.getKlasa();
        default:
            return tmpCar.getiloscWystapien();
        }
    }

    @Override
    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

}
