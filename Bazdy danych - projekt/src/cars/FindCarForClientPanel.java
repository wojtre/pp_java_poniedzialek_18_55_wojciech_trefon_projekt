package cars;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import klasa.KlasaSamochodu;
import klasa.KlasaSamochoduDAO;

public class FindCarForClientPanel extends JPanel {
    private JTextField textFieldMarka;
    private JTextField textFieldModel;
    JComboBox<KlasaSamochodu> cbKlasa = new JComboBox<>();
    JSpinner spinnerRokProdukcji = new JSpinner();
    JRadioButton rdbtnRokProdukcji = new JRadioButton("Rok produkcji");
    JLabel lblMarka = new JLabel("Marka");
    JLabel lblModel = new JLabel("Model");
    private JTable table;
    JButton btnSzukaj = new JButton("Szukaj");
    SamochodyDAO samochodyDAO;
    private final JRadioButton rdbtnKlasa = new JRadioButton("Klasa");
    JScrollPane scrollPane = new JScrollPane();
    List<Samochod> cars;

    private void hideColumns(int... index) {
        for (int i : index) {
            table.getColumnModel().getColumn(i).setMinWidth(0);
            table.getColumnModel().getColumn(i).setMaxWidth(0);
        }
    }

    /**
     * Create the panel.
     */
    public FindCarForClientPanel() {
        try {
            samochodyDAO = new SamochodyDAO();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error: " + e, "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
        setLayout(null);

        textFieldMarka = new JTextField();
        textFieldMarka.setBounds(30, 35, 117, 20);
        add(textFieldMarka);
        textFieldMarka.setColumns(10);

        textFieldModel = new JTextField();
        textFieldModel.setColumns(10);
        textFieldModel.setBounds(174, 35, 117, 20);
        add(textFieldModel);
        spinnerRokProdukcji.setModel(new SpinnerNumberModel(new Integer(2014),
                new Integer(0), null, new Integer(1)));

        spinnerRokProdukcji.setBounds(334, 35, 83, 20);
        spinnerRokProdukcji.setEnabled(false);
        add(spinnerRokProdukcji);

        cbKlasa.setBounds(446, 35, 117, 20);
        List<KlasaSamochodu> st = new ArrayList<KlasaSamochodu>();
        try {
            st = new KlasaSamochoduDAO().getAllKlasySamochodu();
        } catch (Exception e) {

        }
        for (KlasaSamochodu s : st) {
            cbKlasa.addItem(s);
        }
        cbKlasa.setEnabled(false);
        add(cbKlasa);
        rdbtnRokProdukcji.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                spinnerRokProdukcji.setEnabled(rdbtnRokProdukcji.isSelected());
            }
        });

        rdbtnRokProdukcji.setBounds(334, 7, 109, 23);
        add(rdbtnRokProdukcji);

        lblMarka.setBounds(31, 11, 69, 14);
        add(lblMarka);

        lblModel.setBounds(174, 10, 46, 14);
        add(lblModel);

        scrollPane.setBounds(10, 80, 952, 334);
        add(scrollPane);

        table = new JTable();
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                JTable tbl = (JTable) arg0.getSource();

                if (arg0.getClickCount() >= 2) {
                    JFrame jFrame = new JFrame();
                    RentCarPanel rentCarPanel = new RentCarPanel();
                    Point p = arg0.getPoint();
                    int row = tbl.rowAtPoint(p);
                    rentCarPanel.insertData(cars.get(row));
                    rentCarPanel.actualCar = cars.get(row);
                    jFrame.getContentPane().add(rentCarPanel);
                    jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    jFrame.setSize(600, 280);
                    jFrame.setVisible(true);
                }
            }
        });
        scrollPane.setViewportView(table);
        btnSzukaj.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                try {
                    int rokProdukcji;
                    int idKlasy;
                    if (!rdbtnRokProdukcji.isSelected()) {
                        rokProdukcji = 0;
                    }
                    else {
                        rokProdukcji = (int) spinnerRokProdukcji.getValue();
                    }
                    if (!rdbtnKlasa.isSelected()) {
                        idKlasy = -1;
                    }
                    else {
                        idKlasy = ((KlasaSamochodu) cbKlasa.getSelectedItem())
                                .getId();
                    }
                    cars = samochodyDAO.searchCarFoClient(
                            textFieldMarka.getText(), textFieldModel.getText(),
                            rokProdukcji, idKlasy);
                    SamochodyTableModel samochodyTableModel = new SamochodyTableModel(
                            cars);
                    table.setModel(samochodyTableModel);
                    hideColumns(0, 3);

                } catch (Exception e) {
                    JOptionPane.showMessageDialog(FindCarForClientPanel.this,
                            "Error: " + e, "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        btnSzukaj.setBounds(603, 34, 89, 23);
        add(btnSzukaj);
        rdbtnKlasa.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cbKlasa.setEnabled(rdbtnKlasa.isSelected());

            }
        });

        rdbtnKlasa.setBounds(454, 7, 109, 23);

        add(rdbtnKlasa);

    }
}
