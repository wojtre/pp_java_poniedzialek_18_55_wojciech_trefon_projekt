package stan;

public class StanSamochodu {
    private int id = -1;
    private String stan = "";

    public StanSamochodu(int id, String stan) {
        this.id = id;
        this.stan = stan;
    }

    public int getId() {
        return id;
    }

    public String getStan() {
        return stan;
    }

    @Override
    public String toString() {
        return stan;
    }

    @Override
    public boolean equals(Object o) {
        return getId() == ((StanSamochodu) o).getId();
    }
}
