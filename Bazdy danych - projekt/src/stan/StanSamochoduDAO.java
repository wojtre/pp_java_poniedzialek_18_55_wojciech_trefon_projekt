package stan;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class StanSamochoduDAO {
    private Connection myConn;

    public StanSamochoduDAO() throws Exception {
        Properties properties = new Properties();
        properties.load(new FileInputStream("bazydanych.properties"));

        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        String dburl = properties.getProperty("dburl");

        myConn = DriverManager.getConnection(dburl, user, password);
    }

    public List<StanSamochodu> getAllStanSamochodu() throws Exception {
        List<StanSamochodu> stany = new ArrayList<>();
        Statement myStmt = null;
        ResultSet myRs = null;

        try {
            myStmt = myConn.createStatement();
            myRs = myStmt.executeQuery("select * from Stany_samochodu");
            while (myRs.next()) {
                StanSamochodu stan = convertRowToStanSamochodu(myRs);
                stany.add(stan);
            }
            return stany;
        } finally {
            myStmt.close();
            myRs.close();
        }
    }

    private StanSamochodu convertRowToStanSamochodu(ResultSet myRs)
            throws SQLException {
        StanSamochodu stanSamochodu = new StanSamochodu(
                myRs.getInt("id_stanu"), myRs.getString("stan"));
        return stanSamochodu;
    }
}