package klasa;

public class KlasaSamochodu {
    private int id = -1;
    private String nazwa = "";
    private double cenaZaDzien = -1;

    public KlasaSamochodu(int id, String nazwa, double cenaZaDzien) {
        this.id = id;
        this.nazwa = nazwa;
        this.cenaZaDzien = cenaZaDzien;
    }

    public int getId() {
        return id;
    }

    public String getNazwa() {
        return nazwa;
    }

    @Override
    public String toString() {
        return nazwa;
    }

    @Override
    public boolean equals(Object o) {
        return getId() == ((KlasaSamochodu) o).getId();
    }
}