package pracownik;

import java.util.List;

import javax.swing.table.AbstractTableModel;

public class NajlepsiPracownicyTableModel extends AbstractTableModel {
    private static final int MIEJSCE = 0;
    private static final int IMIE = 1;
    private static final int NAZWISKO = 2;
    private static final int PRZYCHOD = 3;

    private String[] columnNames = { "Miejsce", "Imie", "Nazwisko", "Przychod" };
    private List<Pracownik> pracownicy;

    public NajlepsiPracownicyTableModel(List<Pracownik> c) {
        pracownicy = c;
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public int getRowCount() {
        return pracownicy.size();
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public Object getValueAt(int row, int col) {
        Pracownik tmpPracownik = pracownicy.get(row);
        switch (col)
        {
        case MIEJSCE:
            return row + 1;
        case IMIE:
            return tmpPracownik.getImie();
        case NAZWISKO:
            return tmpPracownik.getNazwisko();
        case PRZYCHOD:
            return tmpPracownik.getPrzychod();
        default:
            return null;
        }
    }

    @Override
    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

}
