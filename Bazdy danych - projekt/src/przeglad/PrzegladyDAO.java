package przeglad;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class PrzegladyDAO {
    private Connection myConn;

    public PrzegladyDAO() throws Exception {
        Properties properties = new Properties();
        properties.load(new FileInputStream("bazydanych.properties"));

        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        String dburl = properties.getProperty("dburl");

        myConn = DriverManager.getConnection(dburl, user, password);
    }

    public List<Przeglad> getAllPrzeglady() throws Exception {
        List<Przeglad> przeglady = new ArrayList<>();
        Statement myStmt = null;
        ResultSet myRs = null;

        try {
            myStmt = myConn.createStatement();
            myRs = myStmt
                    .executeQuery("select Samochody_id_samochodu, data_przegladu, koniec_przegladu, marka, model_samochodu, nr_rejestracyjny, "
                            + "id_samochodu from przeglady P inner join samochody S "
                            + "on P.Samochody_id_samochodu = S.id_samochodu");
            while (myRs.next()) {
                Przeglad przeglad = convertRowToPrzeglad(myRs);
                przeglady.add(przeglad);
            }
            return przeglady;
        } finally {
            myStmt.close();
            myRs.close();
        }
    }

    public List<Przeglad> getPrzegladyApToMonths(int months) throws Exception {
        List<Przeglad> przeglady = new ArrayList<>();
        Statement myStmt = null;
        ResultSet myRs = null;

        try {
            myStmt = myConn.createStatement();
            myRs = myStmt
                    .executeQuery("select Samochody_id_samochodu, data_przegladu, koniec_przegladu, marka, "
                            + "model_samochodu, nr_rejestracyjny, id_samochodu from przeglady P "
                            + "inner join samochody S on P.Samochody_id_samochodu = S.id_samochodu "
                            + "where koniec_przegladu<adddate(CURRENT_DATE, interval "
                            + months + " month)");
            while (myRs.next()) {
                Przeglad przeglad = convertRowToPrzeglad(myRs);
                przeglady.add(przeglad);
            }
            return przeglady;
        } finally {
            myStmt.close();
            myRs.close();
        }
    }

    private Przeglad convertRowToPrzeglad(ResultSet myRs) throws SQLException {
        Przeglad przeglady = new Przeglad(
                myRs.getInt("Samochody_id_samochodu"),
                myRs.getString("data_przegladu"),
                myRs.getString("koniec_przegladu"),
                myRs.getInt("id_samochodu"), myRs.getString("marka"),
                myRs.getString("model_samochodu"),
                myRs.getString("nr_rejestracyjny"));
        return przeglady;
    }
}
