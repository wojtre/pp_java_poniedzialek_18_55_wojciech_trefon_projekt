package przeglad;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;

public class PrzegladyPanel extends JPanel {
    JLabel lblMonths = new JLabel("Ilosc miesiecy do przegladu:");
    private JTable table;
    JButton btnSzukaj = new JButton("Szukaj");
    PrzegladyDAO przegladyDAO;
    JScrollPane scrollPane = new JScrollPane();
    List<Przeglad> przeglady;
    private final JSpinner spinner = new JSpinner();

    /**
     * Create the panel.
     */
    public PrzegladyPanel() {
        try {
        	przegladyDAO = new PrzegladyDAO();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error: " + e, "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
        setLayout(null);

        lblMonths.setBounds(31, 11, 171, 14);
        add(lblMonths);

        spinner.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
        spinner.setBounds(212, 8, 54, 20);
        
        scrollPane.setBounds(10, 41, 767, 306);
        add(scrollPane);

        table = new JTable();
        scrollPane.setViewportView(table);
        btnSzukaj.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                try {
                	int nMonths = (int) spinner.getValue();
                    przeglady = przegladyDAO.getPrzegladyApToMonths(nMonths);
                    PrzegladyTableModel przegladyTableModel = new PrzegladyTableModel(przeglady);
                    table.setModel(przegladyTableModel);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(PrzegladyPanel.this, "Error: "
                            + e, "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        btnSzukaj.setBounds(305, 7, 89, 23);
        add(btnSzukaj);
        
        
        add(spinner);
        
        JButton btnShowAll = new JButton("Pokaz wszystkie");
        btnShowAll.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		 try {
                     przeglady = przegladyDAO.getAllPrzeglady();
                     PrzegladyTableModel przegladyTableModel = new PrzegladyTableModel(przeglady);
                     table.setModel(przegladyTableModel);
                 } catch (Exception e1) {
                     JOptionPane.showMessageDialog(PrzegladyPanel.this, "Error: "
                             + e1, "Error", JOptionPane.ERROR_MESSAGE);
                 }
             }
        });
        btnShowAll.setBounds(420, 7, 135, 23);
        add(btnShowAll);

    }
}
