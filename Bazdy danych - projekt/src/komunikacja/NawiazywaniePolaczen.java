package komunikacja;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class NawiazywaniePolaczen extends Thread {
	ArrayList<Socket> connections;
	ServerSocket server;
	
	public NawiazywaniePolaczen(ServerSocket serwer, ArrayList<Socket> lista)
	{
		connections = lista;
		server = serwer;
	}
	public void run()
	{
		while (true)
		{
			Socket sock;
			try {
				sock = server.accept();
				connections.add(sock);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
