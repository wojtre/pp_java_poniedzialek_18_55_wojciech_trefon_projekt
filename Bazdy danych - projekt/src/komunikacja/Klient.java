package komunikacja;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Klient {
	String identyfikator;
	private Socket sc;
	private String host;
	private int port;
	private PrintWriter output;
	private BufferedReader input; 
	
	public boolean isSocketOpen()
	{
		if (sc == null)
			return false;
		
		return true;
	}
	
	public String getIdentifikator(){
		return identyfikator;
	}
	
	public Klient(String nazwaKlienta, String serverHost, int serverPort) throws UnknownHostException, IOException
	{
		identyfikator = nazwaKlienta;
		host = serverHost;
		port = serverPort;
		sc = new Socket(host,port);
		output=new PrintWriter(sc.getOutputStream(),true);
		
	}
	public void send(String message) throws IOException
	{ 
		 output.println(message);
	}
	public String receive() throws IOException
	{
		String all = "";
		String line = "";
	
		input = new BufferedReader(new InputStreamReader(sc.getInputStream())); 
		
		while (input.ready())
		{
			line = input.readLine();
			all+= line + '\n';
		}
		return all;
		
	}
	public void endCommunication() throws IOException
	{
		input.close();
		output.close();
	}
}