package komunikacja;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;

public class Serwer {
    private String localIP;
    private int port;
    private ArrayList<Socket> klienci;
    private PrintWriter output;
    private ServerSocket server;
    private BufferedReader input;
    private NawiazywaniePolaczen watekNawiazywaniaPolaczen;
    private LinkedHashSet<String> klienciNazwy = new LinkedHashSet<>();
    public HashMap<String, Integer> idKlientow = new HashMap<>();

    public String getLocalIP() {
        return localIP;
    }

    public int getPort() {
        return port;
    }

    public Serwer(int serverPort) throws IOException {
        port = serverPort;
        localIP = InetAddress.getLocalHost().getHostAddress();
        server = new ServerSocket(port);
        klienci = new ArrayList<Socket>();
        watekNawiazywaniaPolaczen = new NawiazywaniePolaczen(server, klienci);
        watekNawiazywaniaPolaczen.start();
    }

    public LinkedHashSet getKlienciNazwy() {
        return klienciNazwy;
    }

    public String pobierzWiadomosci() throws IOException {
        String line = "";
        String klientMessage = "";
        String nazwa;
        String all = "";
        int i = 0;

        for (Socket sc : klienci) {
            klientMessage = "";
            input = new BufferedReader(new InputStreamReader(
                    sc.getInputStream()));

            while (input.ready()) {
                line = input.readLine();
                klientMessage += line + '\n';
            }
            if (klientMessage.indexOf(' ') > -1) { // Check if there is more
                                                   // than one
                // word.
                nazwa = klientMessage.substring(0, klientMessage.indexOf(' ')); // Extract
                                                                                // first
                // word.
            }
            else {
                nazwa = klientMessage; // Text is the first word itself.
            }
            if (!nazwa.equals("")) {
                if (idKlientow.containsKey(nazwa)) {
                    idKlientow.remove(nazwa);

                }
                idKlientow.put(nazwa, i);

                klienciNazwy.add(nazwa);

            }
            all += klientMessage;
            i++;
        }

        return all;
    }

    public void WyslijDoKlienta(int index, String message) throws IOException {
        Socket sc = klienci.get(index);
        output = new PrintWriter(sc.getOutputStream());
        output.println(message);
        output.flush();
    }
}
