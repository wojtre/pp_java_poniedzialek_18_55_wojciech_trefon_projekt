package klient;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class KlientDAO {
    private Connection myConn;

    public KlientDAO() throws Exception {
        Properties properties = new Properties();
        properties.load(new FileInputStream("bazydanych.properties"));

        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        String dburl = properties.getProperty("dburl");

        myConn = DriverManager.getConnection(dburl, user, password);
    }

    public List<Klient> getAllKlienci() throws Exception {
        List<Klient> klienci = new ArrayList<>();
        Statement myStmt = null;
        ResultSet myRs = null;

        try {
            myStmt = myConn.createStatement();
            myRs = myStmt.executeQuery("select * from klienci");
            while (myRs.next()) {
                Klient klient = convertRowToKlienci(myRs);
                klienci.add(klient);
            }
            return klienci;
        } finally {
            myStmt.close();
            myRs.close();
        }
    }

    private Klient convertRowToKlienci(ResultSet myRs) throws SQLException {
        Klient klient = new Klient(myRs.getInt("id_klienta"),
                myRs.getString("imie"), myRs.getString("nazwisko"),
                myRs.getString("nr_telefonu"), myRs.getString("login"),
                myRs.getString("haslo"));
        return klient;
    }

    public void addWypozyczenie(int idSamochodu, String since, String to,
            int idKlienta) throws Exception {
        Statement myStmt = null;

        try {

            myStmt = myConn.createStatement();
            myStmt.executeUpdate("INSERT INTO wypozyczenia (poczatek,koniec,oplata_koncowa,Samochody_id_samochodu,Pracownicy_id_pracownika,Klienci_id_klienta) VALUES ( "
                    + since
                    + ", "
                    + to
                    + ", null,"
                    + idSamochodu
                    + ", null, "
                    + idKlienta + ")");
        } finally {
            myStmt.close();
        }
    }
    
    public void addKlient(Klient client) throws Exception {
        Statement myStmt = null;
        try {
            myStmt = myConn.createStatement();
            myStmt.executeUpdate("insert into klienci (imie, nazwisko, nr_telefonu, login, haslo) values "
            		+" ('" +  client.getImie() +"', '" + client.getNazwisko() + "','" + client.getNrTelefonu() +"', '"+client.getLogin()+"', '"+client.getHaslo()+"')");
        } finally {
            myStmt.close();
        }
    }
}
