package klient;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import pracownik.PracownicyDAO;
import pracownik.Pracownik;
import all.Login;

public class NowyKlientFrame {

    public JFrame frame;
    private JTextField textImie;
    private JTextField textNazwisko;
    private JTextField textTelefon;
    private JTextField textLogin;
    private JPasswordField textHaslo;

    public NowyKlientFrame() {
        initialize();
    }

    private void addNewClient() {
        String imie, nazwisko, nrTelefonu, login, haslo;
        imie = textImie.getText();
        nazwisko = textNazwisko.getText();
        nrTelefonu = textTelefon.getText();
        login = textLogin.getText();
        haslo = textHaslo.getText();

        if (imie.isEmpty() || nazwisko.isEmpty() || nrTelefonu.isEmpty()
                || login.isEmpty() || haslo.isEmpty()) {
            JOptionPane.showMessageDialog(null,
                    "Nie wprowadzono wszystkich danych osobowych.");
        }
        else {
            haslo = Login.hashMD5(haslo);
            if (sprawdzLogowanie(login)) {
                try {
                    KlientDAO klientDAO = new KlientDAO();
                    Klient klient = new Klient(imie, nazwisko, nrTelefonu,
                            login, haslo);
                    klientDAO.addKlient(klient);
                    JOptionPane
                            .showMessageDialog(null,
                                    "Rejestracja przebiegla pomyslnie. Od teraz mozna sie zalogowac do systemu");
                    frame.dispose();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {
                JOptionPane.showMessageDialog(null,
                        "Podany login jest zajety. Prosze podac inny login.");
            }
        }
    }

    public boolean sprawdzLogowanie(String login) {
        List<Pracownik> pracownicy = new ArrayList<>();
        List<Klient> klienci = new ArrayList<>();

        try {
            pracownicy = new PracownicyDAO().getAllPracownicy();
            klienci = new KlientDAO().getAllKlienci();

            for (Pracownik p : pracownicy) {
                if (login.equals(p.getLogin()))
                    return false;
            }

            for (Klient k : klienci) {
                if (login.equals(k.getLogin()))
                    return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 420, 460);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JLabel lblPodajDaneOsobowe = new JLabel("Podaj dane osobowe");
        lblPodajDaneOsobowe.setFont(new Font("Tahoma", Font.PLAIN, 15));
        lblPodajDaneOsobowe.setBounds(132, 23, 155, 19);
        frame.getContentPane().add(lblPodajDaneOsobowe);

        JLabel lblImi = new JLabel("Imi\u0119:");
        lblImi.setBounds(43, 62, 46, 14);
        frame.getContentPane().add(lblImi);

        JLabel lblNazwisko = new JLabel("Nazwisko:");
        lblNazwisko.setBounds(43, 87, 68, 14);
        frame.getContentPane().add(lblNazwisko);

        JLabel lblNrTelefonu = new JLabel("Nr telefonu:");
        lblNrTelefonu.setBounds(43, 112, 68, 14);
        frame.getContentPane().add(lblNrTelefonu);

        JLabel lblLogin = new JLabel("Login:");
        lblLogin.setBounds(43, 137, 46, 14);
        frame.getContentPane().add(lblLogin);

        JLabel lblHaso = new JLabel("Has\u0142o:");
        lblHaso.setBounds(43, 162, 46, 14);
        frame.getContentPane().add(lblHaso);

        JLabel lblPozostaeDaneOsobowe = new JLabel(
                "Pozosta\u0142e dane osobowe zostan\u0105 zebrane przez pracownika");
        lblPozostaeDaneOsobowe.setBounds(25, 315, 369, 25);
        frame.getContentPane().add(lblPozostaeDaneOsobowe);

        JLabel lblPodczasPierwszegoOdbioru = new JLabel(
                "podczas pierwszego odbioru samochodu");
        lblPodczasPierwszegoOdbioru.setBounds(77, 342, 293, 14);
        frame.getContentPane().add(lblPodczasPierwszegoOdbioru);

        textImie = new JTextField();
        textImie.setBounds(160, 59, 127, 20);
        frame.getContentPane().add(textImie);
        textImie.setColumns(10);

        textNazwisko = new JTextField();
        textNazwisko.setBounds(160, 84, 127, 20);
        frame.getContentPane().add(textNazwisko);
        textNazwisko.setColumns(10);

        textTelefon = new JTextField();
        textTelefon.setBounds(160, 109, 127, 20);
        frame.getContentPane().add(textTelefon);
        textTelefon.setColumns(10);

        textLogin = new JTextField();
        textLogin.setBounds(160, 134, 127, 20);
        frame.getContentPane().add(textLogin);
        textLogin.setColumns(10);

        textHaslo = new JPasswordField();
        textHaslo.setBounds(160, 159, 127, 20);
        frame.getContentPane().add(textHaslo);

        JButton btnRejestruj = new JButton("Rejestruj");
        btnRejestruj.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                addNewClient();
            }
        });
        btnRejestruj.setBounds(132, 215, 127, 53);
        frame.getContentPane().add(btnRejestruj);
    }
}