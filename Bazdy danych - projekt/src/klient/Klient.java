package klient;

public class Klient {
    private int id = -1;
    private String imie = "";
    private String nazwisko = "";
    private String nrTelefonu = "";
    private String login = "";
    private String haslo = "";

    public Klient(int id, String imie, String nazwisko, String nrTelefonu, String login, String haslo) {
        this.id = id;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.nrTelefonu = nrTelefonu;
        this.login = login;
        this.haslo = haslo;
    }
    
    public Klient(String imie, String nazwisko, String nrTelefonu, String login, String haslo) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.nrTelefonu = nrTelefonu;
        this.login = login;
        this.haslo = haslo;
    }

    @Override
    public String toString() {
        return imie + " " + nazwisko; 
    }
    
    public int getId(){
    	return id;
    }
    
    public String getImie(){
    	return imie;
    }
    
    public String getNazwisko(){
    	return nazwisko;
    }
    
    public String getLogin(){
    	return login;
    }
    
    public String getHaslo(){
    	return haslo;
    }
    
    public String getNrTelefonu(){
    	return nrTelefonu;
    }
}
