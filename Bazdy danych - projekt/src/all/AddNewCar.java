package all;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

public class AddNewCar extends JPanel {

    private JLabel lMarka = new JLabel("Marka");
    private JLabel lModel = new JLabel("Model");
    private JLabel lRokProdukcji = new JLabel("Rok produkcji");
    private JLabel lNrRejestracyjny = new JLabel("Nr rejestracyjny");
    private JLabel lRodzajSilnika = new JLabel("Rodzaj silnika");
    private JLabel lMoc = new JLabel("Moc");
    private JLabel lKlasa = new JLabel("Klasa");

    private JTextField txtMarka = new JTextField();
    private JTextField txtModel = new JTextField();
    private JTextField txtNrRejestracyjny = new JTextField();

    private JSpinner sRokProdukcji = new JSpinner(new SpinnerNumberModel(1, 1,
            Integer.MAX_VALUE, 1));

    public AddNewCar() {

    }
}