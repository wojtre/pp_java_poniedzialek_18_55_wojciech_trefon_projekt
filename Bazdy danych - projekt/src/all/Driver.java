package all;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Driver {
    public Connection connection = null;
    public Statement statement = null;

    public void connectToDatabase(String url, String userName, String password) {
        try {
            connection = DriverManager.getConnection(url, userName, password);
            statement = connection.createStatement();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getMostOftenRentedCars(int numberOfCars, boolean sortAscend) {
        String typeOfSorting = "ASC";
        String cars = String.format("%-5s  %-10s  %-10s  %-10s  \n", "Ilosc",
                "Marka", "Model", "Silnik");
        if (!sortAscend) {
            typeOfSorting = "DESC";
        }
        String query = "select count(T1.Samochody_id_samochodu) as ilosc,  marka, model_samochodu as model, rodzaj_silnika as silnik "
                + "from wypozyczenia T1 inner join samochody T2 on T2.id_samochodu = T1.Samochody_id_samochodu group by "
                + "T1.Samochody_id_samochodu order by ilosc "
                + typeOfSorting
                + " limit " + numberOfCars;
        try {

            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                cars += String.format("%-5s  %-10s  %-10s  %-10s  \n",
                        result.getString("ilosc"), result.getString("marka"),
                        result.getString("model"), result.getString("silnik"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cars;
    }

    public boolean insertToTable(String tableName, List<String> columns,
            List<String> values) {
        String query = "insert into `" + tableName + "` (`" + columns.get(0)
                + "`";
        for (int i = 1; i < columns.size(); i++) {
            query += ", `" + columns.get(i) + "`";
        }
        query += ") values ('" + values.get(0) + "'";
        for (int i = 1; i < values.size(); i++) {
            if (values.get(i).equals("NULL")) {
                query += ", " + values.get(i) + "";
            }
            else {
                query += ", '" + values.get(i) + "'";
            }
        }
        query += ");";
        try {
            statement.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean insertToTable(String tableName,
            String columsSeparatedByComa, String valuesSeparatedByComa) {
        return insertToTable(tableName,
                Arrays.asList(columsSeparatedByComa.split(",")),
                Arrays.asList(valuesSeparatedByComa.split(",")));
    }

    public List<String[]> getCarClasses() {
        String query = "SELECT id_klasy, nazwa FROM klasy_samochodow;";
        ArrayList<String[]> classesArray = new ArrayList<String[]>();
        try {
            ResultSet classes = statement.executeQuery(query);
            while (classes.next()) {
                classesArray.add(new String[] { classes.getString("id_klasy"),
                        classes.getString("nazwa") });
            }
        } catch (Exception e) {
            return null;
        }
        return classesArray;
    }

    public static void main(String[] args) {
        Driver driver = new Driver();
        driver.connectToDatabase("jdbc:mysql://127.0.0.1:3306/bazy", "root",
                "admin");
        // System.out.println(driver.getMostOftenRentedCars(3, false));
        // if (!driver
        // .insertToTable(
        // "wypozyczenia",
        // "poczatek,koniec,oplata_koncowa,Samochody_id_samochodu,Pracownicy_id_pracownika,Klienci_id_klienta",
        // "2015-05-01 00:00:00,2012-12-12 00:00:00,NULL,7,2,3")) {
        // System.out.println("blad");
        // }
        List<String[]> test = driver.getCarClasses();
        for (String[] s : test) {
            System.out.println(s[0] + "   " + s[1]);
        }
    }
}
