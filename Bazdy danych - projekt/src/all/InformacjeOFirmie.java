package all;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import klasa.KlasaSamochodu;
import klasa.KlasaSamochoduDAO;
import java.awt.Font;

public class InformacjeOFirmie extends JPanel {
    private final JLabel lblWojciechTrefon = new JLabel("WOJCIECH TREFON & DAMIAN GADECKI");
    private final JLabel lblDaneKontaktowe = new JLabel("Dane kontaktowe");
    private final JLabel lblAdres = new JLabel("Adres:");
    private final JLabel lblNibylandiaUl = new JLabel("Nibylandia, ul. Warszawska 47");
    private final JLabel lblNibylandia = new JLabel("99-099 Nibylandia");
    private final JLabel lblEmail = new JLabel("E-mail:");
    private final JLabel lblRazdwatrzyiautomasztynibycom = new JLabel("razdwatrzyiautomaszty@niby.com");
    private final JLabel lblTelefon = new JLabel("Telefon:");
    private final JLabel label = new JLabel("+001 435 098 999");

    public InformacjeOFirmie() {
        setLayout(null);
        lblWojciechTrefon.setFont(new Font("Tahoma", Font.BOLD, 18));
        lblWojciechTrefon.setBounds(41, 11, 433, 75);
        
        add(lblWojciechTrefon);
        lblDaneKontaktowe.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblDaneKontaktowe.setBounds(41, 83, 135, 29);
        
        add(lblDaneKontaktowe);
        lblAdres.setBounds(41, 123, 46, 14);
        
        add(lblAdres);
        lblNibylandiaUl.setBounds(107, 123, 224, 14);
        
        add(lblNibylandiaUl);
        lblNibylandia.setBounds(107, 148, 114, 14);
        
        add(lblNibylandia);
        lblEmail.setBounds(41, 174, 46, 14);
        
        add(lblEmail);
        lblRazdwatrzyiautomasztynibycom.setBounds(107, 174, 224, 14);
        
        add(lblRazdwatrzyiautomasztynibycom);
        lblTelefon.setBounds(41, 199, 46, 14);
        
        add(lblTelefon);
        label.setBounds(107, 199, 148, 14);
        
        add(label);
    }
}
