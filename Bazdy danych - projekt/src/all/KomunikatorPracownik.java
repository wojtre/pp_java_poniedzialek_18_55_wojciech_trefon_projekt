package all;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import komunikacja.Serwer;

public class KomunikatorPracownik extends JPanel {

    /**
     * Create the panel.
     */
    private Serwer server;
    private JTextArea txtrRozmowa;
    private JTextArea txtrWiadomosc;
    JComboBox<String> comboBoxNazwaKlienta = new JComboBox();

    public KomunikatorPracownik() {
        setLayout(null);
        try {
            server = new Serwer(5200);
        } catch (IOException e1) {
            server = null;
        }

        JButton btnWyslijWiadomosc = new JButton("Wyslij Wiadomosc");
        btnWyslijWiadomosc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String message = "";
                try {
                    message = txtrWiadomosc.getText();
                    server.WyslijDoKlienta(server.idKlientow
                            .get(comboBoxNazwaKlienta.getSelectedItem()),
                            message);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                txtrRozmowa.setText(txtrRozmowa.getText() + '\n' + "Ty >>"
                        + "  [do "
                        + (String) comboBoxNazwaKlienta.getSelectedItem()
                        + "]  " + message + "\n");
                txtrWiadomosc.setText("");
            }
        });
        btnWyslijWiadomosc.setBounds(604, 286, 258, 69);
        add(btnWyslijWiadomosc);

        JButton btnOdswiez = new JButton("Odswiez");
        btnOdswiez.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                String str = "";
                try {
                    str = server.pobierzWiadomosci();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    str = "blad pobierania";
                }
                txtrRozmowa.setText(txtrRozmowa.getText() + str);
                comboBoxNazwaKlienta.removeAllItems();
                for (Object nazwa : server.getKlienciNazwy()) {
                    comboBoxNazwaKlienta.addItem((String) nazwa);
                }
            }
        });
        btnOdswiez.setBounds(604, 37, 258, 69);
        add(btnOdswiez);

        JLabel lblWiadomosc = new JLabel("Nowa wiadomosc:");
        lblWiadomosc.setFont(new Font("Tahoma", Font.PLAIN, 16));
        lblWiadomosc.setBounds(25, 251, 172, 14);
        add(lblWiadomosc);

        JLabel lblRozmowa = new JLabel("Rozmowa:");
        lblRozmowa.setFont(new Font("Tahoma", Font.PLAIN, 16));
        lblRozmowa.setBounds(25, 12, 172, 14);
        add(lblRozmowa);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(25, 37, 544, 199);
        add(scrollPane);

        txtrRozmowa = new JTextArea();
        txtrRozmowa.setEditable(false);
        scrollPane.setViewportView(txtrRozmowa);

        JScrollPane scrollPane_1 = new JScrollPane();
        scrollPane_1.setBounds(25, 276, 544, 92);
        add(scrollPane_1);

        txtrWiadomosc = new JTextArea();
        scrollPane_1.setViewportView(txtrWiadomosc);

        JButton btnWyczysc = new JButton("Wyczysc");
        btnWyczysc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                txtrRozmowa.setText("");
            }
        });
        btnWyczysc.setBounds(604, 142, 258, 69);
        add(btnWyczysc);

        JLabel lblIp = new JLabel("Brak  Polaczenia");
        lblIp.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblIp.setBounds(25, 399, 320, 25);
        add(lblIp);

        comboBoxNazwaKlienta.setBounds(604, 366, 162, 20);
        add(comboBoxNazwaKlienta);
        if (server == null) {
            lblIp.setText("Chat nie dziala");
            btnWyczysc.setEnabled(false);
            btnWyslijWiadomosc.setEnabled(false);
            btnOdswiez.setEnabled(false);
        }
        else {
            lblIp.setText(server.getLocalIP() + ":" + server.getPort());
        }

    }
}
