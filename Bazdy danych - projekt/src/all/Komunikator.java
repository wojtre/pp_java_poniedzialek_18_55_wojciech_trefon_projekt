package all;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import komunikacja.Klient;

public class Komunikator extends JPanel {

    private JTextArea txtrRozmowa;
    private JTextArea txtrWiadomosc;
    private Klient client;
    private JTextField txtIP;
    private JTextField txtPort;
    private JButton btnOdswiez;
    private JButton btnWyslijWiadomosc;
    private JButton btnWyczysc;
    private JButton btnPolacz;
    private String login;

    public Komunikator(String logName) {
        setLayout(null);

        login = logName;
        btnWyslijWiadomosc = new JButton("Wyslij Wiadomosc");
        btnWyslijWiadomosc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (client != null) {
                    try {
                        String message = "brak";
                        String identyfikator = client.getIdentifikator();
                        message = txtrWiadomosc.getText();
                        client.send(login + " >> " + message);
                        message += '\n';
                        txtrRozmowa.setText(txtrRozmowa.getText() + "Ty>> "
                                + message);
                        txtrWiadomosc.setText("");
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
                else
                    txtrRozmowa.setText(txtrRozmowa.getText() + '\n'
                            + "Blad wysylania, klient nie istnieje");

            }

        });

        btnWyslijWiadomosc.setBounds(604, 286, 258, 69);
        add(btnWyslijWiadomosc);

        btnOdswiez = new JButton("Odswiez");
        btnOdswiez.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                String message = "";
                try {
                    message = client.receive();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (!message.equals(""))
                    txtrRozmowa.setText(txtrRozmowa.getText() + "Serwer >> "
                            + message);
            }
        });
        btnOdswiez.setBounds(604, 37, 258, 69);
        add(btnOdswiez);

        JLabel lblWiadomosc = new JLabel("Nowa wiadomosc:");
        lblWiadomosc.setFont(new Font("Tahoma", Font.PLAIN, 16));
        lblWiadomosc.setBounds(25, 251, 172, 14);
        add(lblWiadomosc);

        JLabel lblRozmowa = new JLabel("Rozmowa:");
        lblRozmowa.setFont(new Font("Tahoma", Font.PLAIN, 16));
        lblRozmowa.setBounds(25, 12, 172, 14);
        add(lblRozmowa);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(25, 37, 544, 199);
        add(scrollPane);

        txtrRozmowa = new JTextArea();
        txtrRozmowa.setFont(new Font("Tahoma", Font.PLAIN, 12));
        txtrRozmowa.setEditable(false);
        scrollPane.setViewportView(txtrRozmowa);

        JScrollPane scrollPane_1 = new JScrollPane();
        scrollPane_1.setBounds(25, 276, 544, 92);
        add(scrollPane_1);

        txtrWiadomosc = new JTextArea();
        txtrWiadomosc.setFont(new Font("Tahoma", Font.PLAIN, 12));
        scrollPane_1.setViewportView(txtrWiadomosc);

        btnWyczysc = new JButton("Wyczysc");
        btnWyczysc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                txtrRozmowa.setText("");
            }
        });
        btnWyczysc.setBounds(604, 142, 258, 69);
        add(btnWyczysc);

        JLabel lblIp = new JLabel("podaj IP serwera");
        lblIp.setFont(new Font("Tahoma", Font.PLAIN, 16));
        lblIp.setBounds(25, 379, 136, 25);
        add(lblIp);

        JLabel lblPort = new JLabel("port");
        lblPort.setFont(new Font("Tahoma", Font.PLAIN, 16));
        lblPort.setBounds(268, 379, 54, 25);
        add(lblPort);

        txtIP = new JTextField();
        txtIP.setText("192.168.1.2");
        txtIP.setFont(new Font("Tahoma", Font.PLAIN, 18));
        txtIP.setBounds(25, 405, 201, 26);
        add(txtIP);
        txtIP.setColumns(10);

        txtPort = new JTextField();
        txtPort.setText("5200");
        txtPort.setFont(new Font("Tahoma", Font.PLAIN, 18));
        txtPort.setBounds(268, 405, 99, 26);
        add(txtPort);
        txtPort.setColumns(10);

        btnPolacz = new JButton("nawiaz polaczenie");
        btnPolacz.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                String IP = "";
                int port = 0;
                String str = "9999";
                try {
                    IP = txtIP.getText();
                    str = txtPort.getText();
                    port = Integer.parseInt(str);

                    client = new Klient("klient1", IP, port);
                } catch (Exception e) {
                    // nic nie rob, niech sie meczy
                }
                if (client != null && client.isSocketOpen() == true) {
                    btnOdswiez.setEnabled(true);
                    btnWyslijWiadomosc.setEnabled(true);
                    btnWyczysc.setEnabled(true);
                    btnPolacz.setEnabled(false);
                }
            }
        });
        btnPolacz.setFont(new Font("Tahoma", Font.PLAIN, 14));
        btnPolacz.setBounds(401, 404, 172, 31);
        add(btnPolacz);
        btnPolacz.setEnabled(true);

        btnOdswiez.setEnabled(false);
        btnWyslijWiadomosc.setEnabled(false);
        btnWyczysc.setEnabled(false);
    }
}
