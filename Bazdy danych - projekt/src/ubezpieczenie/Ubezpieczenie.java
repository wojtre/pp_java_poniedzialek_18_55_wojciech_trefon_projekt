package ubezpieczenie;

import cars.Samochod;

public class Ubezpieczenie {
    private int id = -1;
    private String poczatekUbezpieczenia = "";
    private String koniecUbezepiczenia = "";
    private String nazwaUbezpieczyciela = "";
    private Samochod samochod;

    public Ubezpieczenie(String poczatekUbezpieczenia, String koniecUbezpieczenia, String nazwaUbezpieczyciela, 
    		int idSamochodu ,String marka, String model, String nrRejestracyjny) {
        this.poczatekUbezpieczenia = poczatekUbezpieczenia;
        this.koniecUbezepiczenia = koniecUbezpieczenia;
        this.nazwaUbezpieczyciela = nazwaUbezpieczyciela;
        this.samochod = new Samochod(idSamochodu, marka, model, nrRejestracyjny);
    }

    public int getId() {
        return id;
    }

    public String getPoczatekUbezpieczenia() {
        return poczatekUbezpieczenia;
    }

    public String getKoniecUbezpieczenia(){
    	return koniecUbezepiczenia;
    }
    
    public Samochod getSamochod(){
    	return samochod;
    }
    
    public String getNazwaUbezpieczyciela(){
    	return nazwaUbezpieczyciela;
    }
    
    public void setPoczatekUbezpieczenia(String poczatekUbezpieczenia){
    	this.poczatekUbezpieczenia = poczatekUbezpieczenia;
    }
    
    public void setKoniecUbezpieczenia(String koniecUbezpieczenia){
    	this.koniecUbezepiczenia = koniecUbezpieczenia;
    }
    
    public void setNazwaUbezpieczyciela(String nazwaUbezpieczyciela){
    	this.nazwaUbezpieczyciela = nazwaUbezpieczyciela;
    }
    
    @Override
    public String toString() {
        return poczatekUbezpieczenia + " - " + koniecUbezepiczenia;
    }

    @Override
    public boolean equals(Object o) {
        return getId() == ((Ubezpieczenie) o).getId();
    }
}
