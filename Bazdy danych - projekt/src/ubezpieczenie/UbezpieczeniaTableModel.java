package ubezpieczenie;

import java.util.List;

import javax.swing.table.AbstractTableModel;

public class UbezpieczeniaTableModel extends AbstractTableModel {
	private static final int POCZATEK_UBEZPIECZENIA = 0;
	private static final int KONIEC_UBEZPIECZENIA = 1;
	private static final int NAZWA_UBEZPIECZYCIELA = 2;
	private static final int MARKA = 3;
	private static final int MODEL = 4;
	private static final int NR_REJESTRACYJNY = 5;
	
	private String[] columnNames = {"Poczatek ubezpieczenia", "Koniec ubezpieczenia","Ubezpieczyciel", "Marka", "Model", "Nr rej."};
	private List<Ubezpieczenie> ubezpieczenia;
	
	public UbezpieczeniaTableModel(List<Ubezpieczenie> u) {
		ubezpieczenia = u;
	} 
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return ubezpieczenia.size();
	}

	@Override
	public String getColumnName(int col){
		return columnNames[col];
	}
	
	@Override
	public Object getValueAt(int row, int col) {
		Ubezpieczenie tmpUbeczpieczenie = ubezpieczenia.get(row);
		switch(col){
		case POCZATEK_UBEZPIECZENIA:
			return tmpUbeczpieczenie.getPoczatekUbezpieczenia();
		case KONIEC_UBEZPIECZENIA:
			return tmpUbeczpieczenie.getKoniecUbezpieczenia();
		case NAZWA_UBEZPIECZYCIELA:
			return tmpUbeczpieczenie.getNazwaUbezpieczyciela();
		case MARKA:
			return tmpUbeczpieczenie.getSamochod().getmarka();
		case MODEL:
			return tmpUbeczpieczenie.getSamochod().getmodel();
		case NR_REJESTRACYJNY:
			return tmpUbeczpieczenie.getSamochod().getnrRejestracyjny();
		default:
			return tmpUbeczpieczenie.getId();
		}
	}
	
	@Override
	public Class getColumnClass(int c){
		return getValueAt(0, c).getClass();
	}

}
