package statystyki;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class PrzychodDAO {
    private Connection myConn;

    public PrzychodDAO() throws Exception {
        Properties properties = new Properties();
        properties.load(new FileInputStream("bazydanych.properties"));

        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        String dburl = properties.getProperty("dburl");

        myConn = DriverManager.getConnection(dburl, user, password);
    }

    public List<Przychod> getPrzychodyMonths(int months) throws Exception {
        List<Przychod> przychody = new ArrayList<>();
        Statement myStmt = null;
        ResultSet myRs = null;

        try {
            myStmt = myConn.createStatement();
            myRs = myStmt
                    .executeQuery("select sum(oplata_koncowa) as przychod, sum(timestampdiff(day,poczatek,koniec)) as dni, count(id_samochodu) as wypozyczen , marka, model_samochodu, id_samochodu , nr_rejestracyjny from wypozyczenia W right outer join samochody S on W.Samochody_id_samochodu=S.id_samochodu  where poczatek>adddate(CURRENT_DATE, interval -"
                            + months
                            + " month) group by id_samochodu order by przychod desc");
            while (myRs.next()) {
                Przychod przychod = convertRowToPrzychod(myRs);
                przychody.add(przychod);
            }
            return przychody;
        } finally {
            myStmt.close();
            myRs.close();
        }
    }

    private Przychod convertRowToPrzychod(ResultSet myRs) throws SQLException {
        Przychod przeglady = new Przychod(myRs.getInt("przychod"),
                myRs.getInt("dni"), myRs.getInt("wypozyczen"),
                myRs.getString("marka"), myRs.getString("model_samochodu"),
                myRs.getInt("id_samochodu"), myRs.getString("nr_rejestracyjny"));
        return przeglady;
    }

    public int getMoneyForMonth(int month) throws Exception {
        int sum = 0;
        Statement myStmt = null;
        ResultSet myRs = null;
        month *= -1;

        try {
            myStmt = myConn.createStatement();
            myRs = myStmt
                    .executeQuery("select sum(oplata_koncowa) as kwota  "
                            + "from wypozyczenia where poczatek between adddate(current_date(), "
                            + "interval " + month
                            + " month) and adddate(current_date(), interval "
                            + (month + 1) + " month)");
            while (myRs.next()) {
                sum = myRs.getInt("kwota");
            }
            return sum;
        } finally {
            myStmt.close();
            myRs.close();
        }
    }
}
