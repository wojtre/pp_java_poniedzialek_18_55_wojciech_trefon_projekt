package statystyki;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import all.Miesiace;

public class PrzychodChartPanel extends JPanel {
    public ArrayList<Integer> przychody = new ArrayList<Integer>();
    public int[] dochod;
    JSpinner spinnerMiesiac = new JSpinner();
    JLabel lblChart = new JLabel("");

    /**
     * Create the panel.
     */
    public PrzychodChartPanel() {
        setLayout(null);

        JLabel lblNewLabel = new JLabel("Przychod za");
        lblNewLabel.setBounds(35, 11, 95, 14);
        add(lblNewLabel);
        spinnerMiesiac.setModel(new SpinnerNumberModel(new Integer(1),
                new Integer(1), null, new Integer(1)));

        spinnerMiesiac.setBounds(140, 8, 47, 20);
        add(spinnerMiesiac);

        JLabel lblOstatnichMiesiecy = new JLabel("ostatnich miesiecy");
        lblOstatnichMiesiecy.setBounds(197, 11, 116, 14);
        add(lblOstatnichMiesiecy);

        JButton btnGeneruj = new JButton("Generuj");
        btnGeneruj.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                countAllPrzychod((int) spinnerMiesiac.getValue());
                lblChart.setIcon(new ImageIcon(printPrzychod(dochod,
                        dochod.length * 100 + 200, 315)));
            }
        });
        btnGeneruj.setBounds(319, 7, 89, 23);
        add(btnGeneruj);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 36, 933, 335);
        add(scrollPane);
        scrollPane.setViewportView(lblChart);

    }

    public BufferedImage printPrzychod(int[] data, int width, int height) {
        // Create a simple XY chart
        DefaultCategoryDataset ds = new DefaultCategoryDataset();
        XYSeries series = new XYSeries("XYGraph");
        for (int i = 0; i < data.length; i++) {
            series.add(i, data[i]);
            if (i == 0) {
                ds.addValue(
                        data[i],
                        "przychod",
                        Miesiace.getMesiac(
                                new GregorianCalendar().getTime().getMonth()
                                        - i).toString()
                                + "(obecny)");
            }
            else {
                ds.addValue(
                        data[i],
                        "przychod",
                        Miesiace.getMesiac(new GregorianCalendar().getTime()
                                .getMonth() - i));
            }
        }

        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series);
        JFreeChart chart = ChartFactory.createBarChart(
                "Przychod na przestrzeni miesiecy", // Title
                "Miesiac", // x-axis Label
                "Przychod", // y-axis Label
                ds, // Dataset
                PlotOrientation.VERTICAL, // Plot Orientation
                false, // Show Legend
                false, // Use tooltips
                false // Configure chart to generate URLs?
                );
        return chart.createBufferedImage(width, height);
    }

    public void countAllPrzychod(int months) {
        dochod = new int[months];
        try {
            PrzychodDAO przychodDAO = new PrzychodDAO();
            for (int i = 0; i < months; i++) {
                dochod[i] = przychodDAO.getMoneyForMonth(i + 1);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error: " + e, "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

}
